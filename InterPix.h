
// C lib stuff
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// windows stuff for timestamp gen
#include <Windows.h>


// include opencv
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

// add the libs here
#ifdef _DEBUG
#pragma comment(lib, "opencv_core310d.lib")
#pragma comment(lib, "opencv_imgproc310d.lib")
#pragma comment(lib, "opencv_imgcodecs310d.lib")
#pragma comment(lib, "opencv_videoio310d.lib")
#pragma comment(lib, "opencv_highgui310d.lib")

#else
#pragma comment(lib, "opencv_core310.lib")
#pragma comment(lib, "opencv_imgproc310.lib")
#pragma comment(lib, "opencv_imgcodecs310.lib")
#pragma comment(lib, "opencv_videoio310.lib")
#pragma comment(lib, "opencv_highgui310.lib")

#endif

// some typedefs for us
typedef unsigned char BYTE;
typedef uint32_t     UINT32;


// internal color as a 4byte 
struct COLOR
{
    union 
    {
        BYTE ColorChannel[4];
        UINT32 ColorFull;
    };
};


// function prototype
cv::Mat DoInterPix(cv::Mat &InputImage);
std::string GetTimeStampString();
std::string type2str(int type);
std::string GetBaseFilename(std::string fName);
std::string GetExtFilename(std::string fName);
bool SafeColorKey (COLOR &check);
COLOR Dissasemble(cv::Vec3b &InputColorVector);



