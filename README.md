# README #

This repository is the c++ version of an old pixel interpolation algorithm I made, specifically for dos games.
Note: the code is not optimized, nor fully commented / explained. It is meant as a simple drag/drop demo. 

## Requirements ##
* OpenCV3 Installed
* VisualStudio 2013 or better
* Windows

## How to Use it ##
* In Windows, drag and drop an image file over the application
* Next, look for a file next to the one that you just passed in, it will have a FILENAME_InterPixTTTTTTTTTTTT.extension format, where TTTTT is a unique timestamp, and FILENAME is your original file


