
// include our header
#include "InterPix.h"

// global handle
HANDLE hConsole;

// magenta color key -> change this if you use a different color
COLOR ColorKey = 
{ //  R   G   B   x 
    {255, 0, 255,128}
};

// fun stuff
void PrintGreetings()
{
    printf("                                                                                              \n");
    printf("                                                                 /[-])//  ___                 \n");
    printf("                                                            __ --\\ `_/~--|  / \\               \n");
    printf("                                                          /_-/~~--~~ /~~~\\\\_\\ /\\              \n");
    printf("                                                          |  |___|===|_-- | \\ \\ \\             \n");
    SetConsoleTextAttribute(hConsole, 8); printf("                  "); SetConsoleTextAttribute(hConsole, 6); printf("_____");
    SetConsoleTextAttribute(hConsole, 8); printf("     "); SetConsoleTextAttribute(hConsole, 6); printf("_____");
    SetConsoleTextAttribute(hConsole, 8);
    printf("       _/~~~~~~~~|~~\\,   ---|---\\___/----|  \\/\\-\\            \n");
    SetConsoleTextAttribute(hConsole, 8); printf("                  "); SetConsoleTextAttribute(hConsole, 12); printf("�����");
    SetConsoleTextAttribute(hConsole, 8); printf("     "); SetConsoleTextAttribute(hConsole, 12); printf("�����");
    SetConsoleTextAttribute(hConsole, 8);
    printf("       ~\\________|__/   / // \\__ |  ||  / | |   | |          \n");
    printf("                                                 ,~-|~~~~~\\--, | \\|--|/~|||  |   | |          \n");
    printf("                                                 [3-|____---~~ _--'==;/ _,   |   |_|          \n");
    printf("                                                             /   /\\__|_/  \\  \\__/--/          \n");
    printf("                                                            /---/_\\  -___/ |  /,--|           \n");
    printf("                                                            /  /\\/~--|   | |  \\///            \n");
    printf("                                                           /  / |-__ \\    |/                  \n");
    printf("                                                          |--/ /      |-- | \\                 \n");
    printf("    _______         __               ______ __           \\^~~\\\\/\\      \\   \\/- _              \n");
    printf("   |_     _|.-----.|  |_.-----.----.|   __ \\__|.--.--.    \\    |  \\     |~~\\~~| \\             \n");
    printf("    _|   |_ |     ||   _|  -__|   _||    __/  ||_   _|     \\    \\  \\     \\   \\  | \\           \n");
    printf("   |_______||__|__||____|_____|__|  |___|  |__||__.__|       \\    \\ |     \\   \\    \\          \n");
    printf("                                                              |~~|\\/\\|     \\   \\   |          \n");
    printf("                             By "); SetConsoleTextAttribute(hConsole, 2);
    printf("Robert Lengyel");                   SetConsoleTextAttribute(hConsole, 8);
    printf(" V1.0          |   |/         \\_--_- |\\         \n");
    printf("                             For "); SetConsoleTextAttribute(hConsole, 6);
    printf("Wolf & Sagaras");                    SetConsoleTextAttribute(hConsole, 8);
    printf("              |  /            /   |/\\/         \n");
    printf("                                                              ~~             /  /             \n");
    printf("                                                                            |__/              \n");
    printf("                                                                                              \n");
}

// main starting point
int main(int argc, char* argv[])
{
    // greetings~!
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 8);
    PrintGreetings();

    // use with command line arguments
    try
    {
        if (argc > 1)
        {
            // get the input filename
            SetConsoleTextAttribute(hConsole, 7);
            std::string FileName = argv[1];
            printf("Reading: ");                SetConsoleTextAttribute(hConsole, 10);
            printf("%s\n", FileName.c_str());   SetConsoleTextAttribute(hConsole, 7);

            // read as an image, and do the interpolation 
            cv::Mat InputImage     = cv::imread(FileName);
            cv::Mat FlipInputImage;  cv::flip(InputImage, FlipInputImage, 0); // this is because of allegro's flipped buffers
            cv::Mat OutputImage    = DoInterPix(FlipInputImage);
            cv::Mat FlipOutputImage; cv::flip(OutputImage, FlipOutputImage, 0); // this is because of allegro's flipped buffers

            // show the stuff?? -> debug only
            //cv::imshow("InputImage", InputImage);
            //cv::imshow("Output", OutputImage);
            //cv::waitKey(0);

            // write the file -> add a timestamp to prevent overwriting
            std::string OutputFilename = GetBaseFilename(FileName) + "_InterPix" + GetTimeStampString() + "." + GetExtFilename(FileName);
            printf("Writing to: ");                 SetConsoleTextAttribute(hConsole, 14);
            printf("%s\n", OutputFilename.c_str()); SetConsoleTextAttribute(hConsole, 7);
            cv::imwrite( OutputFilename, FlipOutputImage );
        
        }   
        else // can't run, no input
        {
            SetConsoleTextAttribute(hConsole, 12);
            printf("Please use this with an image file....\n");
        }
    }
    catch (const cv::Exception& e) // catch opencv crap
    {
        // absorb and exit
        SetConsoleTextAttribute(hConsole, 12);
        printf("RUNTIME OPENCV::EXCEPTION: possibly from a bad input file...%s\n", e.what());
    }
    catch (const std::exception &e) // catch anything else
    {
        // absorb and exit
        SetConsoleTextAttribute(hConsole, 12);
        printf("RUNTIME STD::EXPCETION: possibly from a bad input file...%s\n", e.what());
    }

    // exit
    SetConsoleTextAttribute(hConsole, 7);
    printf("Done....Closing in 5 seconds\n");
    Sleep(5000);
    return 0;
}

// checks againt the color key Magenta
bool SafeColorKey (COLOR &check)
{
	if ( check.ColorChannel[0] == ColorKey.ColorChannel[0] && 
         check.ColorChannel[1] == ColorKey.ColorChannel[1] && 
         check.ColorChannel[2] == ColorKey.ColorChannel[2] )
	return false;
	else return true;
}

// junk helper function
COLOR Dissasemble(cv::Vec3b &InputColorVector) // right values will be modified
{
	COLOR target;
    target.ColorChannel[0] = InputColorVector[0];
    target.ColorChannel[1] = InputColorVector[1];
    target.ColorChannel[2] = InputColorVector[2];
    target.ColorChannel[3] = 255;
	return target;
}

// does the interpolation
cv::Mat DoInterPix(cv::Mat &InputImage) //int x, int y, int type)
{
    // make the output picture
    cv::Mat InterpolatedPicture(InputImage.size().height, InputImage.size().width, CV_8UC3);
    InterpolatedPicture = cv::Scalar(ColorKey.ColorChannel[0], ColorKey.ColorChannel[1], ColorKey.ColorChannel[2]); // clear it 

    // some color constants
	COLOR top, topright, topleft, bottem, bottemright, bottemleft, left, right, middle;
    

	// interpolate
	for(int y=1; y<InputImage.size().height-1; y++)
	{
		for(int x=1; x<InputImage.size().width-1; x++)
		{
			// look up, look down, look all around, your fly's open
            COLOR diss_col;
			middle          = Dissasemble( InputImage.at<cv::Vec3b>(y, x)     ); // self
			top 	        = Dissasemble( InputImage.at<cv::Vec3b>(y+1, x)   );
			topright        = Dissasemble( InputImage.at<cv::Vec3b>(y+1, x+1) );
			topleft         = Dissasemble( InputImage.at<cv::Vec3b>(y+1, x-1) );
			bottem 	        = Dissasemble( InputImage.at<cv::Vec3b>(y-1, x)   );
			bottemright     = Dissasemble( InputImage.at<cv::Vec3b>(y-1, x+1) );
			bottemleft      = Dissasemble( InputImage.at<cv::Vec3b>(y-1, x-1) );
			left 	        = Dissasemble( InputImage.at<cv::Vec3b>(y, x-1)   );
            right 	        = Dissasemble( InputImage.at<cv::Vec3b>(y, x+1)   );

            // go through the channels
			for(int ii=0; ii<3; ii++)
			{
                // always just in case
                diss_col.ColorChannel[ii] = middle.ColorChannel[ii]; 

                // check againt the color key
                if(SafeColorKey(middle) == true)
				{
					if(SafeColorKey(top)    == false && SafeColorKey(right) == false) diss_col.ColorChannel[ii] = top.ColorChannel[ii];
					if(SafeColorKey(top)    == false && SafeColorKey(left)  == false) diss_col.ColorChannel[ii] = top.ColorChannel[ii];
					if(SafeColorKey(bottem) == false && SafeColorKey(right) == false) diss_col.ColorChannel[ii] = bottem.ColorChannel[ii];
					if(SafeColorKey(bottem) == false && SafeColorKey(left)  == false) diss_col.ColorChannel[ii] = bottem.ColorChannel[ii];

					if (middle.ColorChannel[ii] == bottem.ColorChannel[ii]
					 && middle.ColorChannel[ii] == right.ColorChannel[ii]
					 && middle.ColorChannel[ii] == bottemright.ColorChannel[ii]
					 && top.ColorChannel[ii] == left.ColorChannel[ii] && SafeColorKey(top) == true && SafeColorKey(left) == true)
					diss_col.ColorChannel[ii] = (top.ColorChannel[ii] + left.ColorChannel[ii] + middle.ColorChannel[ii]) / 3;

					if (middle.ColorChannel[ii] == bottem.ColorChannel[ii]
					 && middle.ColorChannel[ii] == left.ColorChannel[ii]
					 && middle.ColorChannel[ii] == bottemleft.ColorChannel[ii]
					 && top.ColorChannel[ii] == right.ColorChannel[ii] && SafeColorKey(top) == true && SafeColorKey(right) == true)
					diss_col.ColorChannel[ii] = (top.ColorChannel[ii] + right.ColorChannel[ii] + middle.ColorChannel[ii]) / 3;

					if (middle.ColorChannel[ii] == top.ColorChannel[ii]
					 && middle.ColorChannel[ii] == left.ColorChannel[ii]
					 && middle.ColorChannel[ii] == topleft.ColorChannel[ii]
					 && bottem.ColorChannel[ii] == right.ColorChannel[ii] && SafeColorKey(bottem) == true && SafeColorKey(right) == true)
					diss_col.ColorChannel[ii] = (bottem.ColorChannel[ii] + right.ColorChannel[ii] + middle.ColorChannel[ii]) / 3;

					if (middle.ColorChannel[ii] == top.ColorChannel[ii]
					 && middle.ColorChannel[ii] == right.ColorChannel[ii]
					 && middle.ColorChannel[ii] == topright.ColorChannel[ii]
					 && bottem.ColorChannel[ii] == left.ColorChannel[ii] && SafeColorKey(bottem) == true && SafeColorKey(left) == true)
					diss_col.ColorChannel[ii] = (bottem.ColorChannel[ii] + left.ColorChannel[ii] + middle.ColorChannel[ii]) / 3;
				}
				else
				{
					if (top.ColorChannel[ii]    == left.ColorChannel[ii])	diss_col.ColorChannel[ii] = left.ColorChannel[ii];
					if (top.ColorChannel[ii]    == right.ColorChannel[ii])	diss_col.ColorChannel[ii] = right.ColorChannel[ii];
					if (bottem.ColorChannel[ii] == left.ColorChannel[ii])	diss_col.ColorChannel[ii] = left.ColorChannel[ii];
					if (bottem.ColorChannel[ii] == right.ColorChannel[ii])  diss_col.ColorChannel[ii] = right.ColorChannel[ii];
				}

			}		

            // output to the image
            InterpolatedPicture.at<cv::Vec3b>(y, x)[0] = diss_col.ColorChannel[0];
            InterpolatedPicture.at<cv::Vec3b>(y, x)[1] = diss_col.ColorChannel[1];
            InterpolatedPicture.at<cv::Vec3b>(y, x)[2] = diss_col.ColorChannel[2];
            //InterpolatedPicture.at<cv::Vec4b>(y, x)[3] = 255;
		}
	}

    // give back the mat for output
    return InterpolatedPicture;

}


// gets a timestamp
std::string GetTimeStampString()
{
    char Temp[64] = {0};
    SYSTEMTIME myTime;    GetLocalTime(&myTime);
    FILETIME myTimeFile;  SystemTimeToFileTime(&myTime, &myTimeFile);
    ULARGE_INTEGER SecondCount;
    SecondCount.LowPart = myTimeFile.dwLowDateTime;
    SecondCount.HighPart = myTimeFile.dwHighDateTime;
    sprintf_s<64>(Temp,  "%llu", SecondCount);
    std::string Output = Temp;
    return Output;
}

// just for helping
//printf("InputType = %s\n", type2str(InputImage.type()).c_str());
std::string type2str(int type)
{
    std::string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch (depth) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
    }

    r += "C";
    r += (chans + '0');

    return r;
}

// finds the base filename without extension
std::string GetBaseFilename(std::string fName)
{
    size_t pos = fName.rfind(".");
    if(pos == std::string::npos)  //No extension.
        return fName;

    if(pos == 0)    //. is at the front. Not an extension.
        return fName;

    return fName.substr(0, pos);
}

// finds the base filename without extension
std::string GetExtFilename(std::string fName)
{
    size_t pos = fName.rfind(".");
    if(pos == std::string::npos)  //No extension.
        return fName;

    if(pos == 0)    //. is at the front. Not an extension.
        return fName;

    return fName.substr(pos+1);
}


